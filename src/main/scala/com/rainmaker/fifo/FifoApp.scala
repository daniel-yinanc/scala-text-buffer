package com.rainmaker.fifo

import java.io.{File, PrintWriter}

import scala.collection.mutable
import scala.io.Source

/*
 Algo: Shared delay buffer
 Read string from file (file.txt on resources folder).. parse to initial string Iterator
 for every device number... add to existing bufferSize before sending
 check if buffer is overflown
  if so, introduce a delay matching next entry of the same device
  use same delay if less than any other delay request (from any other device), otherwise put difference as new delay
 in the end write buffer to a file called out.txt at execution root path.
 
 Further optimization potential exists on putting longest delay devices to the bottom of the text file,
 assuming linear processing however re-parsing gigabit length text files are performance exhaustive as well.
 */
object FifoApp extends App {
  val computerToDeviceImpedance:Int = 4
  var delayIntroduced: Int = 0
  val deviceTrafficSize: Int = 250000000
  var initialString: Iterator[String] = Iterator()
  var bufferMap: mutable.HashMap[Int, Int] = mutable.HashMap()
  var fileBuffer: List[String] = List()
  
  initialString = loadFromFile()
  
  for(lines <- initialString){
    val arrStr: Array[String] = lines.split(" ")
    if(bufferMap.contains(arrStr(0).toInt)){
      if(bufferMap(arrStr(0).toInt) + arrStr(1).toInt <= deviceTrafficSize){
        bufferMap(arrStr(0).toInt) = bufferMap(arrStr(0).toInt) + arrStr(1).toInt
        sendToDevice(arrStr(0).toInt, arrStr(1).toInt)
      } else {
        if(bufferMap(arrStr(0).toInt) + arrStr(1).toInt - deviceTrafficSize <= delayIntroduced){
          bufferMap(arrStr(0).toInt) = bufferMap(arrStr(0).toInt) + arrStr(1).toInt
          sendToDevice(arrStr(0).toInt, arrStr(1).toInt)
        } else {
          val newDelayIntroduced = bufferMap(arrStr(0).toInt) + arrStr(1).toInt - deviceTrafficSize - delayIntroduced
          delayIntroduced = delayIntroduced + newDelayIntroduced
          bufferMap(arrStr(0).toInt) = bufferMap(arrStr(0).toInt) + arrStr(1).toInt
          sendToDevice(-1, newDelayIntroduced * computerToDeviceImpedance)
          sendToDevice(arrStr(0).toInt, arrStr(1).toInt)
        }
      }
    } else {
      if(arrStr(1).toInt <= deviceTrafficSize){
        bufferMap += (arrStr(0).toInt -> arrStr(1).toInt)
        sendToDevice(arrStr(0).toInt, arrStr(1).toInt)
      }
    }
  }
  
  writeToFile(fileBuffer.toIterator)
  
  def loadFromFile():
    Iterator[String] ={
    Source.fromURL(getClass.getResource("/file.txt")).getLines.seq
  }
  
  def sendToDevice(deviceNbr:Int, commandLength:Int):
  Unit ={
    fileBuffer = fileBuffer ++ List( deviceNbr + " " + commandLength)
  }
  
  def writeToFile(iteratorStr: Iterator[String]):
  Unit ={
    val pw = new PrintWriter(new File("out.txt" ))
    for(str <- iteratorStr){
      pw.write(str)
      pw.write("\n")
    }
    pw.close()
  }
}
