
#  Shared delay buffer
## Algorithm
1. Read string from file (file.txt on resources folder).. parse to initial string Iterator
2. For every device number... add to existing bufferSize before sending
  * check if buffer is overflown
    * if so, introduce a delay matching next entry of the same device
    * use same delay if less than any other delay request (from any other device), otherwise put difference as new delay
3. Write buffer to a file called out.txt at execution root path.

## Optimization Notes
Further optimization potential exists on putting longest delay devices to the bottom of the text file,
assuming linear processing however re-parsing gigabit length text files are performance exhaustive as well.

# Install and Run
## Step by step instructions
1. Ensure TypeSafe Activator is installed and operational.
2. Modify file.txt on src/main/resources folder as input
3. type "activator run" press enter at root folder (where build.sbt is located)
4. out.txt will be produced with re-ordered entries with introduced delay
